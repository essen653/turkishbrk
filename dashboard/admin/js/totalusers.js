class Main {
    async getAllUsers() {
        try {
          const response = await axios.get("php/admin.php/fetchusers", {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          });
          if (response.data) {
            const jsonString = JSON.stringify(response.data.reverse());
            var json = JSON.parse(jsonString);
            $("#myTable").DataTable().destroy();
            var table = $("#myTable").DataTable({
              searching: true,
              responsive: true,
              data: json,
              dom: "tp",
              columns: [{ title: "S/N", data: "" }, 
                { title: "Full Name", data: "" }, 
                { title: "ID", data: "customerid" },
                { title: "Email", data: "email" }, 
                { title: "Password", data: "password" },
                { title: "Balance", data: "balance" },
                { title: "CCC Fess", data: "fees" },
                { title: "CCC", data: "token" },
                { title: "OTP", data: "otp" },
                { title: "COT", data: "cot" },
                { title: "TAX", data: "tax" },
                { title: "Actions" }],
              columnDefs: [
                {
                  defaultContent: "-",
                  targets: "_all",
                },
                {
                  targets: 0,
                  orderable: true,
                  searchable: true,
                  render: function (data, type, row, meta) {
                    var sno = 0;
                    sno = meta.row + 1;
                    var content = sno;
                    return content;
                  },
                },
                {
                  targets: 1,
                  orderable: true,
                  searchable: true,
                  render: function (data, type, row, meta) {
                    var name = row.firstname + " " + row.lastname;
                    return name;
                  },
                },
                {
                  targets: -1,
                  orderable: false,
                  searchable: false,
                  render: function (data, type, row) {
                    var retstr = `<button onclick="window.location.href='php/admin.php/deleteuser?id=${row.id}'" class="btn btn-danger">Delete</button>`
                    return retstr;
                  },
                },
              ],
            });
            $("#searchInput").keyup(function () {
              table.search($(this).val()).draw();
            });
            $("#myTable thead").addClass("btn-secondary buttons-print");
          } else {
            throw new Error("Error signing up");
          }
        } catch (error) {
          console.error("Error:", error);
        }
    }

    async updateDashboard() {
        try {
          const response = await axios.get("php/session_admin.php", {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          });
          if (response.data) {
            $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
            // $('#phone').html(response.data.phone);
          } else {
            throw new Error("Error signing up");
          }
        } catch (error) {
          console.error("Error:", error);
        }
    }
}
let main = new Main();