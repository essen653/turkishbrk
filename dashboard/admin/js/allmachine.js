fetch("../admin/php/admin.php/getMachineDetails")

.then(function(response){
    return response.json();
})
.then(function(users){
    let placeholder = document.querySelector("#machines");

    let out = "";
    for(let user of users) {
        out += `
            <tr>
                <td>${user.machine_name}</td>
                <td>${user.price}</td>
                <td>${user.income}</td>
                <td>${user.bought}</td>
            </tr>
        `;
    }

    placeholder.innerHTML = out;
})
