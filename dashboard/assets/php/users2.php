<?php
require_once 'config.php';
use PHPMailer\PHPMailer\PHPMailer;
require 'PHPMailer-master/PHPMailer-master/src/PHPMailer.php';
require 'PHPMailer-master/PHPMailer-master/src/SMTP.php';
require 'PHPMailer-master/PHPMailer-master/src/Exception.php';
class jobsAPI{
    private $sqlConn;
    private $users;
    private $myAsset;
    private $t_Table;
    private $new_user;
    private $d_board;
    private $message;
    private $ref_bonus;
    private $deposit;

    public function __construct($usersTable, $usersAsset, $tTable, $new_user_Table, $dashboard, $chat, $bonus, $pending, $conn){
        $this->sqlConn = $conn;
        $this->users = $usersTable;
        $this->myAsset = $usersAsset;            
        $this->t_Table = $tTable;
        $this->new_user = $new_user_Table;
        $this->d_board = $dashboard;  
        $this->message = $chat;  
        $this->ref_bonus = $bonus;     
        $this->deposit = $pending;
    }
    public function randomToken($length = 16) {
        $characters = '1234567890';
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $token;
    }
    public function resetToken($length = 4) {
        $characters = '1234567890';
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $token;
    }
    public function ref_ID($length = 6) {
        $characters = '1234567890ABCDES';
        $token = '';
        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $token;
    }
    public function sendMail($subject, $body, $email) {
        $mail = new PHPMailer(true);
        // $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = 'isbrkonline.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@isbrkonline.com';
        $mail->Password = 'Blackdiamon@2';
        $mail->Port = 465; 
        $mail->SMTPSecure = 'ssl';
        
        $mail->setFrom('noreply@isbrkonline.com', 'isbrkonline'); 
        $mail->addAddress($email); 
        
        $mail->Subject = $subject;
        $mail->Body = $body;
        
        if ($mail->send()) {
            return true;
        } 
        else {
            return false;
        }
    }
    public function time() {
        $timezone = new DateTimeZone('GMT');
        $currentDateTime = new DateTime('now', $timezone);
        $currentDateTime->modify('+1 hour');

        $formattedDateTime = $currentDateTime->format('Y-m-d H:i:s');

        return $formattedDateTime;
    }
    public function forgotPassword($email, $token){
        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $update = "UPDATE $this->users SET `reset_token` = ? WHERE `email` = ?";
            $query = $this->sqlConn->prepare($update);
            $query->bind_param("ss", $token, $email);
            if ($query->execute()) {
                return true;
            }
        }
    }
    public function updatePassword($token, $password){
        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE reset_token = ?");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->num_rows > 0) {
            $row1 = $result->fetch_assoc();
            $id = $row1['id'];
            $empty = '';
            $update = "UPDATE $this->users SET `password` = ?, `reset_token` = ? WHERE `id` = ?";
            $query = $this->sqlConn->prepare($update);
            $query->bind_param("sss", $password, $empty, $id);
            if ($query->execute()) {
                return true;
            }
        }
    }
    public function handleRequest($method, $endpoint, $data){
        switch ($endpoint) {
            case '/sendMailToNewUser':
                if ($method === 'GET') {
                    $stmt = $this->sqlConn->prepare("SELECT * FROM $this->new_user LIMIT 1");
                    $stmt->execute();
                    $machine_result = $stmt->get_result();

                    if ($machine_result->num_rows == 1) {
                        $row1 = $machine_result->fetch_assoc();
                        $firstname = $row1['firstname'];
                        $email = $row1['email'];

                        $subject = 'Welcome to isbrkonline';
                        $body = 'Hello ' . $firstname . ', ' .
                        "\n" .
                        "\n" .
                        "We welcome you to our world’s leading hashpower provider! It’s super simple - The fastest Dx mining hardware are already set up and running. " .
                        "\n" .
                        "As soon as you’ve set up your account and buy your rigs from the machine store, it will generate revenue at every 12 hours.\n" .
                        "\n" .
                        "We are passionate about Dx and blockchain technology. " .
                        "Our Dx Mining services are accessible to everyone. We and our team believe in the future of cryptocurrency & proud to be a part of this fast-growing community.\n" .
                        "\n" .
                        "Visit us at https://isbrkonline.com for more information about us.\n" .
                        "\n" .
                        "CEO isbrkonline.\n" .
                        "20-22 Wenlock Road, London, England, N1 7GU";
                        $sendmail = $this->sendMail($subject, $body, $email);
                        if ($sendmail) {
                            $stmt = $this->sqlConn->prepare("DELETE FROM $this->new_user WHERE email = ?");
                            $stmt->bind_param("s", $email);
                            if ($stmt->execute()) {
                                return [
                                    'message' => 'Email sent',
                                ];
                            }
                        }
                    } else {
                        return [
                            'message' => 'No new user',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request methods.',
                    ];
                }
                break;
            
            case '/sendToken':
                if ($method === 'POST') {
                    if (isset($_POST['email'])){ 
                        $email = $_POST['email'];
                        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE email = ?");
                        $stmt->bind_param("s", $email);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        if ($result->num_rows > 0) {
                            $row = $result->fetch_assoc();
                            $token = $this->resetToken();
                            $update = $this->forgotPassword($email, $token);
                            if ($update) {
                                $email = $row['email'];
                                $firstname = $row['firstname'];
                                $subject = 'Password reset token';
                                $body = 'Dear ' . $firstname . ', ' .
                                "\n" .
                                "\n" .
                                "Password reset was initiated and below is your token:" .
                                "\n" .
                                "\n" .
                                "\n" .
                                $token .
                                "\n" .
                                "\n" .
                                "\n" .
                                "Should you have any questions or concerns, please contact us at" .
                                "\n" .
                                "support@isbrkonline.com" .
                                "\n" .
                                "\n" .
                                "\n" .
                                "Or visit us at https://isbrkonline.com for more information about us.\n" .
                                "\n" .
                                "CEO isbrkonline.\n" .
                                "20-22 Wenlock Road, London, England, N1 7GU";
                                $sendmail = $this->sendMail($subject, $body, $email);
                                if ($sendmail) {
                                    return [
                                        'message' => 'Successful',
                                    ];
                                }
                            } else {
                                return [
                                    'message' => 'Password reset failed. Please confirm your Token',
                                ];
                            }
                        } else {
                            return [
                                'message' => 'Sorry there\'s no such email',
                            ]; 
                        }
                    } else {
                        return [
                            'message' => 'Missing parameters',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request method.',
                    ];
                }
                break;
            case '/confirmToken':
                if ($method === 'POST') {
                    if (isset($_POST['token']) && isset($_POST['password1'])){ 
                        $token = $_POST['token'];
                        $password = $_POST['password1'];
                        $stmt = $this->sqlConn->prepare("SELECT * FROM $this->users WHERE reset_token = ?");
                        $stmt->bind_param("s", $token);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        if ($result->num_rows > 0) {
                            $row = $result->fetch_assoc();
                            $token = $row['reset_token'];
                            $update = $this->updatePassword($token, $password);
                            if ($update) {
                                return [
                                    'message' => 'Successful',
                                ];
                            } else {
                                return [
                                    'message' => 'Failed. Confirm Token',
                                ];
                            }
                        } else {
                            return [
                                'message' => 'Invalid token.',
                            ]; 
                        }
                    } else {
                        return [
                            'message' => 'Missing parameters',
                        ];
                    }
                } else {
                    return [
                        'message' => 'Invalid request',
                    ];
                }
                break;
            default:
                return 'Invalid endpoint';
        }
    }
    
}

$usersTable = 'users';
$usersAsset = 'usersasset';
$tTable = 'transactions';
$new_user_Table = 'new_user_queue';
$dashboard = 'dashboard';
$chat = 'message';
$bonus = 'referrers';
$pending = 'deposit';


$api = new jobsAPI($usersTable, $usersAsset, $tTable, $new_user_Table, $dashboard, $chat, $bonus, $pending, $conn);

$method = $_SERVER['REQUEST_METHOD'];
$endpoint = parse_url($_SERVER['PATH_INFO'], PHP_URL_PATH);
$endpoint = rtrim($endpoint, '/');



$data = $_POST; 

// var_dump($data);
$response = $api->handleRequest($method, $endpoint, $data);

// Set the appropriate headers
header('Content-Type: application/json');

// Send the response
echo json_encode($response);

