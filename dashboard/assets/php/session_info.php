<?php
session_start();

header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

if (isset($_SESSION['customerid'])) {
    include 'config.php';
    $id = $_SESSION['customerid'];
    $sql = "SELECT * FROM `users` WHERE customerid = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows == 1) {
        $row = $result->fetch_assoc();
        $balance = $row['balance'];
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $fullname = $row['fullname'];
        $id2 = $row['id'];
        $phone = $row['phone'];
        $email = $row['email'];
        $transaction = $row['transaction'];
        $account_number = $row['account_number'];
        $fees = $row['fees'];
        $total_deposit = $row['total_deposit'];

        $sql2 = "SELECT * FROM `transactions` WHERE user_id = ?";
        $stmt2 = $conn->prepare($sql2);
        $stmt2->bind_param("i", $id2);
        $stmt2->execute();
        $result2 = $stmt2->get_result();
        if ($result2) {
            $row2 = $result2->fetch_all(MYSQLI_ASSOC);
        }

        $sql3 = "SELECT * FROM `admin` WHERE id = ?";
        $id3 = 1;
        $stmt3 = $conn->prepare($sql3);
        $stmt3->bind_param("i", $id3);
        $stmt3->execute();
        $result3 = $stmt3->get_result();
        $row3 = $result3->fetch_assoc();
        $wallet = $row3['wallet'];

        $userDetails = array(
            'user_id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'fullname' => $fullname,
            'phone' => $phone,
            'email' => $email,
            'id' => $id,
            'balance' => $balance,
            'account_number' => $account_number,
            'fees' => $fees,
            'transaction' => $transaction,
            'total_deposit' => $total_deposit,
            'transactions' => $row2,
            'wallet' => $wallet
        );

        header('Content-Type: application/json');
        echo json_encode($userDetails);
    } else {
        echo json_encode(array('error' => 'user id not found'));
    }
} else {
    echo json_encode(array('error' => 'Not authorized'));
}
?>
