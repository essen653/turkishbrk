<?php
session_start();
include "config.php";

function validate($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['customerid']) && isset($_POST['password'])) {
        $customerid = validate($_POST['customerid']);
        $pass = validate($_POST['password']);

        $sql1 = "SELECT * FROM `admin` WHERE customerid = ?";
        $stmt1 = $conn->prepare($sql1);
        $stmt1->bind_param("s", $customerid);
        $stmt1->execute();
        $result1 = $stmt1->get_result();

        if ($result1->num_rows == 1) {
            $row1 = $result1->fetch_assoc();
            if ($pass == $row1['password']) {
                session_start();
                $_SESSION['fullname'] = $row1['firstname'] . " " . $row1['lastname'];
                $_SESSION['customerid'] = $row1['customerid'];
                $_SESSION['email'] = $row1['email'];
                $_SESSION['id'] = $row1['id'];
                echo 1;
            } else {
                echo "Customerid or password is incorrect";
            }
        } else {
            $sql = "SELECT * FROM `users` WHERE customerid = ?";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("s", $customerid);
            $stmt->execute();
            $result = $stmt->get_result();

            if ($result->num_rows == 1) {
                $row = $result->fetch_assoc();
                if ($pass == $row['password']) {
                    session_start();
                    $_SESSION['phone'] = $row['phone'];
                    $_SESSION['fullname'] = $row['firstname'] . " " . $row['lastname'];
                    $_SESSION['customerid'] = $row['customerid'];
                    $_SESSION['id'] = $row['id'];
                    echo 0;
                } else {
                    echo "Customerid or password is incorrect";
                }
            } else {
                echo "Customerid or password is incorrect";
            }
        }
    }
}
?>
