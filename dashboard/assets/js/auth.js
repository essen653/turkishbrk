let loginBtn = document.getElementById("loginbtn");
let signUpBtn = document.getElementById("signupbtn");
let resetPasswordBtn = document.getElementById("resetPasswordBtn");
// function toggleSpinner(action) {
//     const textSpan = document.querySelector(".textSpan");
//     const spinnerSpan = document.querySelector(".spinnerSpan");
//     let parentElement = spinnerSpan.parentElement;
//     if (action === "show") {
//         textSpan.style.display = "none";
//         spinnerSpan.style.display = "inline-block";
//         parentElement.setAttribute("disabled", true);
//     } else if (action === "hide") {
//         textSpan.style.display = "inline-block";
//         spinnerSpan.style.display = "none";
//         parentElement.removeAttribute("disabled");
//     }
// };
function toggleSpinner(action, button) {
    let textSpan, spinnerSpan;

    if (button === "signupbtn") {
        textSpan = document.querySelector(".textSpan2");
        spinnerSpan = document.querySelector(".spinnerSpan2");
    } else {
        textSpan = document.querySelector(".textSpan");
        spinnerSpan = document.querySelector(".spinnerSpan");
    }

    let parentElement = spinnerSpan.parentElement;

    if (action === "show") {
        textSpan.style.display = "none";
        spinnerSpan.style.display = "inline-block";
        parentElement.setAttribute("disabled", true);
    } else if (action === "hide") {
        textSpan.style.display = "inline-block";
        spinnerSpan.style.display = "none";
        parentElement.removeAttribute("disabled");
    }
}

function togglePasswordVisibility(inputId, showIconId, hideIconId) {
    const passwordInput = document.getElementById(inputId);
    const showPasswordIcon = document.getElementById(showIconId);
    const hidePasswordIcon = document.getElementById(hideIconId);

    if (passwordInput.type === "password") {
        passwordInput.type = "text";
        showPasswordIcon.style.display = "none";
        hidePasswordIcon.style.display = "block";
    } else {
        passwordInput.type = "password";
        hidePasswordIcon.style.display = "none";
        showPasswordIcon.style.display = "block";
    };
}
function showErrorToast(message) {
    iziToast.error({
        title: 'Error',
        message: message,
        position: 'topRight'
    });
}

function showSuccessToast(message) {
    iziToast.success({
        title: 'OK',
        message: message,
        position: 'topRight'
    });
}


function signUp() {
    toggleSpinner("hide", "signupbtn");
    let firstName = document.querySelector('input[name="fname"]').value;
    let lastName = document.querySelector('input[name="lname"]').value;
    let username = document.querySelector('input[name="username2"]').value;
    let email = document.querySelector('input[name="email"]').value;
    let gender = document.getElementById('gender').value;
    let pwd = document.querySelector('input[name="password2"]').value;
    let pwd2 = document.querySelector('input[name="cpassword"]').value;

    if (firstName == "") {
        showErrorToast("Firstname is required");
    } else if (firstName.length < 3) {
        showErrorToast("Firstname is too short");
    } else if (lastName == "") {
        showErrorToast("Lastname is required");
    } else if (lastName.length < 3) {
        showErrorToast("Lastname is too short");
    } else if (username == "") {
        showErrorToast("Username is required");
    } else if (username.length < 3) {
        showErrorToast("username is too short");
    } else if (email == "") {
        showErrorToast("Email is required");
    } else if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email) == false) {
        showErrorToast("Email is not valid");
    } else if (gender == "") {
        showErrorToast("Gender is required");
    } else if (pwd == "") {
        showErrorToast("Password is required");
    } else if (pwd.length < 4) {
        showErrorToast("Password is too short");
    } else if (pwd2 == "") {
        showErrorToast("Password doesn't match");
    } else if (pwd !== pwd2) {
        showErrorToast("Password doesn't match");
    } else {
        toggleSpinner("show", "signupbtn");
        const signupData = new URLSearchParams();

        signupData.append('firstname', firstName);
        signupData.append('lastname', lastName);
        signupData.append('email', email);
        signupData.append('username', username);
        signupData.append('password', pwd);
        signupData.append('gender', gender);

        axios.post('https://api.cybev.org/register', signupData.toString())
            .then(response => {
                toggleSpinner("hide", "signupbtn");
                return response.data;
            }).then((data) => {
                toggleSpinner("hide", "signupbtn");
                showSuccessToast('Registration Successful');
                setTimeout(() => {
                    window.location.href = 'login';
                }, 2000);
            })
            .catch(error => {
                var errmsg = error.response.data.message;
                toggleSpinner("hide", "signupbtn");
                iziToast.error({
                    title: 'Error',
                    message: errmsg,
                    position: 'topRight'
                });

            });
    }

}

function login() {
    toggleSpinner("hide");
    let userid = document.querySelector('input[name="username"]').value;
    let username = userid.toLowerCase();
    let pwd = document.querySelector('input[name="password"]').value;
    if (userid == "") {
        iziToast.error({
            title: 'Error',
            message: "Username is required",
            position: 'topRight'
        });
    } else if (userid.length < 3) {
        iziToast.error({
            title: 'Error',
            message: "Username is too short",
            position: 'topRight'
        });
    } else if (pwd == "") {
        iziToast.error({
            title: 'Error',
            message: "Password is required",
            position: 'topRight'
        });
    } else if (pwd.length < 4) {
        iziToast.error({
            title: 'Error',
            message: "Password is too short",
            position: 'topRight'
        });
    } else {
        toggleSpinner("show");
        const loginData = new URLSearchParams();
        loginData.append('username', username);
        loginData.append('password', pwd);

        axios.post('https://api.cybev.org/login', loginData.toString())
            .then(response => {
                setItem('authToken', response.data.message);
                window.location.href = "/";
            })
            .catch(error => {
                iziToast.error({
                    title: 'Error',
                    message: error.response.data.message,
                    position: 'topRight'
                });
                toggleSpinner("hide");
            });
    }
};


function resetPassword() {
    toggleSpinner("hide");
    let email = document.querySelector('input[name="email"]').value;
    // let token = getItem('authToken');
    if (email == "") {
        iziToast.error({
            title: 'Error',
            message: "email is required",
            position: 'topRight'
        });
    } else {
        toggleSpinner("show");
        const formData = new URLSearchParams();
        formData.append('email', email);

        axios.post('https://api.cybev.org/reset-password', formData.toString())
            .then(response => {
                console.log(response)
                if (response.data.code === 200) {
                    window.location.href = "/change-password"
                }
            })
            .catch(error => {
                console.error('AJAX Error:', error);
                toggleSpinner("hide");
                let errmsg = error.response.data.message
                iziToast.error({
                    title: 'Error',
                    message: errmsg,
                    position: 'topRight'
                });
            });
    }
}

